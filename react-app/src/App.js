import './App.css';
import { PageProduct } from './components/PageProduct/PageProduct'

function App() {
  return (
    <div className="App">
      <PageProduct />
    </div>
  );
}

export default App;
