import React from 'react';
import './PageProduct.css';
import apple_touch_icon from './images/apple-touch-icon.png';
import ico_basket_2 from './images/ico-basket-2.svg';
import image_1 from './images/image-1.webp';
import image_2 from './images/image-2.webp';
import image_3 from './images/image-3.webp';
import image_4 from './images/image-4.webp';
import image_5 from './images/image-5.webp';
import color_1 from './images/color-1.webp';
import color_2 from './images/color-2.webp';
import color_3 from './images/color-3.webp';
import color_4 from './images/color-4.webp';
import color_5 from './images/color-5.webp';
import color_6 from './images/color-6.webp';
import review_1 from './images/review-1.jpeg';
import review_2 from './images/review-2.jpeg';
import star_4 from './images/star-4.png';
import star_5 from './images/star-5.png';

export const PageProduct = () => {
	return (
		<body className="body">
			<p className="p__padding"><a name="top"></a></p>
			<header className="header-sticky">
				<div className="hat">
					<div className="hat__header">
						<div>
							<img className="hat__ico" src={apple_touch_icon} alt="Символ МойМаркета" />
							<span className="hat__name hat__color">Мой</span><span className="hat__name">Маркет</span>
						</div>
						<div className="basket">
							<img className="basket__ico" src={ico_basket_2} alt="Иконка корзины" />
							<div className="basket__count js-basket-count"></div>
						</div>
					</div>
				</div>
			</header>
			<main>
				<nav className="breadcrumb breadcrumb__left">
					<a className="breadcrumb" href="/">Электроника</a>
					<span>🠖</span>
					<a className="breadcrumb" href="/">Смартфоны и гаджеты</a>
					<span>🠖</span>
					<a className="breadcrumb" href="/">Мобильные телефоны</a>
					<span>🠖</span>
					<a className="breadcrumb" href="/">Apple</a>
				</nav>
				<h2 className="smartphone">Смартфон Apple iPhone 13, синий</h2>
				<div className="smartphone__photo smartphone__left">
					<img className="smartphone__photo" src={image_1} alt="Фотография смартфона. Экран и задняя сторона." />
					<img className="smartphone__photo" src={image_2} alt="Фотография смартфона. Экран." />
					<img className="smartphone__photo" src={image_3} alt="Фотография смартфона. Экран и задняя сторона, вид под углом." />
					<img className="smartphone__photo" src={image_4} alt="Фотография смартфона. Камеры крупным планом." />
					<img className="smartphone__photo" src={image_5} alt="Фотография смартфона. Задняя сторона." />
				</div>
				<div className="product">
					<div>
						<h3 className="product-color__name">Цвет товара: синий</h3>
						<div className="product-color__button-imgs">
							<button className="product-color__button"><img className="product-color__button-img" src={color_1} alt="Фотография смартфона. Красный." /></button>
							<button className="product-color__button"><img className="product-color__button-img" src={color_2} alt="Фотография смартфона. Серый." /></button>
							<button className="product-color__button"><img className="product-color__button-img" src={color_3} alt="Фотография смартфона. Розовый." /></button>
							<button className="product-color__button product-color__button_selected"><img className="product-color__button-img" src={color_4} alt="Фотография смартфона. Синий." /></button>
							<button className="product-color__button"><img className="product-color__button-img" src={color_5} alt="Фотография смартфона. Белый." /></button>
							<button className="product-color__button"><img className="product-color__button-img" src={color_6} alt="Фотография смартфона. Черный." /></button>
						</div>
						<h3 className="product-config__name">Конфигурация памяти: 128 ГБ</h3>
						<div className="product-config__buttons">
							<button className="product-config__button product-config__button_selected">128 ГБ</button>
							<button className="product-config__button">256 ГБ</button>
							<button className="product-config__button">512 ГБ</button>
						</div>
						<h3 className="product-characteristic__name">Характеристики товара</h3>
						<div className="product-characteristic">
							<ul className="product-characteristic__attributes">
								<li className="product-characteristic__attribute">Экран: <b>6.1</b></li>
								<li className="product-characteristic__attribute">Встроенная память: <b>128 ГБ</b></li>
								<li className="product-characteristic__attribute">Операционная система: <b>iOS 15</b></li>
								<li className="product-characteristic__attribute">Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
								<li className="product-characteristic__attribute">Процессор: <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank"><b>Appel A15 Bionic</b></a></li>
								<li className="product-characteristic__attribute">Вес: <b>173 г</b></li>
							</ul>
						</div>
						<h3 className="product-description__name">Описание</h3>
						<div className="product-description">
							<p className="product-description__paragraph">Наша самая совершенная система двух камер.
							<br />Особый взгляд на прочность дисплея.
							<br />Чип, с которым всё супербыстро.
							<br />Аккумулятор держится заметно дольше.
							<br /><i>iPhone 13 - сильный мира всего.</i>
							</p>
							<p className="product-description__paragraph">Мы разработали совершенно новую схему расположения и развернули объективы на 45 градусов. Благодаря этому внутри корпуса поместилась наша лучшая система двух камер с увеличенной матрицей широкоугольной камеры. Кроме того, мы освободили место для системы оптической стабилизациии изображения сдвигом матрицы. И повысили скорость работы матрицы на сверхширокоугольной камере.</p>
							<p className="product-description__paragraph">Новая сверхширокоугольная камера видит больше деталей в тёмных областях снимков. Новая широкоугольная камера улавливает на 47% больше света для более качественных фотографий и видео. Новая оптическая стабилизация со сдвигом матрицы обеспечит чёткие кадры даже в неустойчивом положении.</p>
							<p className="product-description__paragraph">Режим «Киноэффект» автоматически добавляет великолепные эффекты перемещения фокуса и изменения резкости. Просто начните запись видео. Режим «Киноэффект» будет удерживать фокус на объекте съёмки, создавая красивый эффект размытия вокруг него. Режим «Киноэффект» распознаёт, когда нужно перевести фокус на другого человека или объект, который появился в кадре. Теперь ваши видео будут смотреться как настоящее кино.</p>
						</div>
						<h3 className="product-table__name">Сравнение моделей</h3>
						<div>
							<div className="product-table__padding">
								<table className="product-table">
									<thead className="product-table__thead">
										<tr>
											<th>Модель</th>
											<th>Вес</th>
											<th>Высота</th>
											<th>Ширина</th>
											<th>Толщина</th>
											<th>Чип</th>
											<th>Объём памяти</th>
											<th>Аккумулятор</th>
										</tr>
									</thead>
									<tbody className="product-table__body">
										<tr>
											<td>Iphone11</td>
											<td>194 грамма</td>
											<td>150.9 мм</td>
											<td>75.7 мм</td>
											<td>8.3 мм</td>
											<td>A13 Bionicchip</td>
											<td>до 128 Гб</td>
											<td>до 17 часов</td>
										</tr>
										<tr>
											<td>Iphone12</td>
											<td>164 грамма</td>
											<td>146.7 мм</td>
											<td>71.5 мм</td>
											<td>7.4 мм</td>
											<td>A14 Bionicchip</td>
											<td>до 256 Гб</td>
											<td>до 19 часов</td>
										</tr>
										<tr>
											<td>Iphone13</td>
											<td>174 грамма</td>
											<td>146.7 мм</td>
											<td>71.5 мм</td>
											<td>7.65 мм</td>
											<td>A15 Bionicchip</td>
											<td>до 512 Гб</td>
											<td>до 19 часов</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<section className="review-section">
							<div className="review-section__header">
								<div className="review-section__wrapper">
									<h3 className="review-section__title">Отзывы</h3>
									<span className="review-section__count">425</span>
								</div>
							</div>
							<div className="review-section__list">
								<div className="review">
									<img className="review__photo" src={review_1} alt="Фотография Марка Г." />
									<div className="review__content">
										<h4 className="review__name">Марк Г.</h4>
										<img className="review__rating" src={star_5} alt="Пять звёзд" />
										<div className="review__parameters">
											<p className="review__parameter"><strong>Опыт использования:</strong> менее месяца</p>
											<p className="review__parameter"><strong>Достоинства:</strong><br />это мой первый айфон после после огромного количества телефонов на андроиде. Всё плавно, чётко и красиво. Довольно шустрое устройство. Камера весьма неплохая, ширик тоже на высоте.</p>
											<p className="review__parameter"><strong>Недостатки:</strong><br />к самому устройству мало имеет отношение, но перенос данных с андроида - адская вещь), а если нужно переносить фото с компа, то это только через itunes, который урезает качество фотографий исходное</p>
										</div>
									</div>
								</div>
							</div>
							<div className="separator__padding">
								<div className="separator"></div>
							</div>
							<div className="review">
								<img className="review__photo" src={review_2} alt="Фотография Валерия Коваленко" />
								<div className="review__content">
									<h4 className="review__name">Валерий Коваленко</h4>
									<img className="review__rating" src={star_4} alt="Четыре звезды" />
									<div className="review__parameters">
										<p className="review__parameter"><strong>Опыт использования:</strong> менее месяца</p>
										<p className="review__parameter"><strong>Достоинства:</strong><br />OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго</p>
										<p className="review__parameter"><strong>Недостатки:</strong><br />Плохая ремонтопригодность</p>
									</div>
								</div>
							</div>
						</section>
						<h3 className="form__name">Добавить свой отзыв</h3>
						<form className="form">
							<div className="form__string">
								<div>
									<input type="text" className="form__text-size form__text" placeholder = "Имя и фамилия" />
									<div className="errorName"></div>
								</div>
								<div>
									<input type="text" className="form__number-size form__number" placeholder = "Оценка"/>
									<div className="errorScore"></div>
								</div>
							</div>
							<textarea className="form__review-size form__review" placeholder = "Текст отзыва"></textarea>
							<button type="submit" className="form__button">Отправить отзыв</button>
						</form>
					</div>
					<aside className="right">
						<div className="important">
							<div className="price-sale">
								<div className="like">
									<div className="price-old">
										<div className="price-old__price">75 990₽</div>
									</div>
								</div>
								<div className="like">
									<div className="sale">
										<div className="sale__percent">-8%</div>
									</div>
								</div>
								<div className="like">
									<div className="ico-like">
										<svg className="ico-like__image" width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill-rule="evenodd" clip-rule="evenodd" d="M3.78502 6.57269C6.17872 4.27474 10.0466 4.27474 12.4403 6.57269L15.0001 9.03017L17.56 6.57269C19.9537 4.27474 23.8216 4.27474 26.2154 6.57269C28.609 8.87064 28.609 12.5838 26.2154 14.8818L15.0001 25.6483L3.78502 14.8818C1.39132 12.5838 1.39132 8.87064 3.78502 6.57269ZM10.6725 8.26974C9.25515 6.90905 6.97018 6.90905 5.55278 8.26974C4.1354 9.63044 4.1354 11.824 5.55278 13.1848L15.0001 22.2542L24.4476 13.1848C25.865 11.824 25.865 9.63044 24.4476 8.26974C23.0302 6.90905 20.7452 6.90905 19.3279 8.26974L15.0001 12.4243L10.6725 8.26974Z" />
										</svg>
									</div>
								</div>
							</div>
							<div className="price-new">67 990₽</div>
							<div className="delivery">
								<span>Самовывоз в четверг, 1 сентября - <b>бесплатно</b></span>
								<span>Курьером в четверг, 1 сентября - <b>бесплатно</b></span>
							</div>
							<button className="basket-button js-add-button">Добавить в корзину</button>
						</div>
						<div className="advertising">Реклама</div>
						<div className="advertising__iframe">
							<iframe title="myIframe" className="iframe" scrolling="no" src="./ads.html"></iframe>
							<iframe title="myIframe" className="iframe" scrolling="no" src="./ads.html"></iframe>
						</div>
					</aside>
				</div>
			</main>
			<footer className="footer">
				<div className="footer__padding1">
					<div className="footer__padding">
						<div>
							<p className="footer__paragrapf">© ООО «<span className="footer__paragrapf-color">Мой</span>Маркет», 2018-2022</p>
							<p className="footer__paragrapf-weight">Для уточнения информации звоните по номеру <a href="tel:79000000000">+7 900 000 0000</a>,</p>
							<p className="footer__paragrapf-weight">а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com">partner@mymarket.com</a></p>
						</div>
						<div className="footer__up"><a href="#top">Наверх</a></div>
					</div>
				</div>
			</footer>
	</body>
	);
}