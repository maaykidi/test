"use strict"

let form = document.querySelector('.form');
let inputName = form.querySelector('.form__text');
let inputScore = form.querySelector('.form__number');
let errorNameElem = form.querySelector('.errorName');
let errorScoreElem = form.querySelector('.errorScore');
let inputReview = form.querySelector('.form__review');

class AddReviewForm {
    constructor({ form, inputName, inputScore, errorNameElem, errorScoreElem, inputReview }) {
        this.form = form;
        this.inputName = inputName;
        this.inputScore = inputScore;
        this.errorNameElem = errorNameElem;
        this.errorScoreElem = errorScoreElem;
        this.inputReview = inputReview;

        this.init();
    }

    init() {
        this.inputName.value = localStorage.getItem('inputName');
        this.inputScore.value = localStorage.getItem('inputScore');
        this.inputReview.value = localStorage.getItem('inputReview');

        this.form.addEventListener('submit', this.handleSubmit.bind(this));
        this.form.addEventListener('change', this.handleFormChange.bind(this))

        this.inputName.addEventListener('focus', () => {
            errorNameElem.classList.remove('visible');
        });

        this.inputScore.addEventListener('focus', () => {
            errorScoreElem.classList.remove('visible');
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        
        let errorName = '';
        let name = inputName.value;
                        
        if (name.length === 0) {
            errorName = 'Вы забыли указать имя и фамилию';
        } else {
            if (name.length < 2) {
                errorName = 'Имя не может быть короче 2-х символов';
            }
        }
            
        errorNameElem.innerText = errorName;
        errorNameElem.classList.toggle('visible', errorName);
        
        if (errorName) return;

        let errorScore = '';

        let score = +inputScore.value;
       
        if (!score) {
            errorScore = 'Оценка должна быть от 1 до 5';
        } else {
            if (score === 1 || score === 2 || score === 3 || score === 4 || score === 5) {
            } else {
                errorScore = 'Оценка должна быть от 1 до 5';
            }
        }

        errorScoreElem.innerText = errorScore;
        errorScoreElem.classList.toggle('visible', errorScore);
            
        let review = inputReview.value;

        if (!errorScore && !errorName) this.clearLocalStorage();
    }

    handleFormChange() {
        let score = inputScore.value;
        let review = inputReview.value;
        let name = inputName.value;
        localStorage.setItem('inputName', name);
        localStorage.setItem('inputScore', score);
        localStorage.setItem('inputReview', review); 
    }

    clearLocalStorage() {
        localStorage.removeItem('inputName');
        localStorage.removeItem('inputScore');
        localStorage.removeItem('inputReview'); 
    }
}

new AddReviewForm({
    form,
    inputName,
    inputScore,
    errorNameElem,
    errorScoreElem,
    inputReview
})