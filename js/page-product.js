"use strict"

let form = document.querySelector('.form');
let inputName = form.querySelector('.form__text');
let inputScore = form.querySelector('.form__number');
let errorNameElem = form.querySelector('.errorName');
let errorScoreElem = form.querySelector('.errorScore');
let inputReview = form.querySelector('.form__review');

function handleSubmit(event) {
    event.preventDefault();
        
    let errorName = '';

    let name = inputName.value;

    if (name.length === 0) {
        errorName = 'Вы забыли указать имя и фамилию';
    } else {
        if (name.length < 2) {
        errorName = 'Имя не может быть короче 2-х символов';
            }
        }
        
        errorNameElem.innerText = errorName;
        errorNameElem.classList.toggle('visible', errorName);
        if (errorName) return;

    let errorScore = '';

    let score = +inputScore.value;

    if (!score) {
        errorScore = 'Оценка должна быть от 1 до 5';
    } else {
        if (score === 1 || score === 2 || score === 3 || score === 4 || score === 5) {
        } else {
            errorScore = 'Оценка должна быть от 1 до 5';
            }
        }

        errorScoreElem.innerText = errorScore;
        errorScoreElem.classList.toggle('visible', errorScore);

    if (!errorName && !errorScore) {
        localStorage.clear();
    }
}

form.addEventListener('change', () => {
    let name = inputName.value;
    let score = inputScore.value;
    let review = inputReview.value;
    localStorage.setItem('inputName', name);
    localStorage.setItem('inputScore', score);
    localStorage.setItem('inputReview', review);
});

inputName.value = localStorage.getItem('inputName');
inputScore.value = localStorage.getItem('inputScore');
inputReview.value = localStorage.getItem('inputReview');

form.addEventListener('submit', handleSubmit);

inputName.addEventListener('focus', () => {
    errorNameElem.classList.remove('visible');
});

inputScore.addEventListener('focus', () => {
    errorScoreElem.classList.remove('visible');
});

// Промежуточная аттестация

let addButton = document.querySelector('.js-add-button');
let basketCount = document.querySelector('.js-basket-count');
let isAdded = localStorage.getItem('isAdded') !== 'false';

let basketText = {
	blank: 'Добавить в корзину',
	added: 'Товар уже в корзине'
}

function increaseBasketCount() {
	basketCount.innerHTML = 1;
}

function decreaseBasketCount() {
	if (basketCount.innerHTML > 0) basketCount.innerHTML = '';
}

addButton.innerHTML = basketText.blank;
basketCount.innerHTML = '';

if (isAdded) increaseBasketCount();

function toggleAddButton() {
	if (isAdded) {
		addButton.classList.add('added');
		addButton.innerHTML = basketText.added;
	} else {
		addButton.classList.remove('added');
		addButton.innerHTML = basketText.blank;
	}
}

function onClickAddButton() {
	if (isAdded) {
		isAdded = false;
		toggleAddButton();
		decreaseBasketCount();
		localStorage.setItem('isAdded', false);
	} else {
		isAdded = true;
		toggleAddButton();
		increaseBasketCount();
		localStorage.setItem('isAdded', true);
	}
}

// addButton.innerHTML.value = localStorage.getItem('addButton.innerHTML');
// basketCount.innerHTML.value = localStorage.getItem('basketCount.innerHTML');

addButton.addEventListener('click', onClickAddButton);
toggleAddButton();